# Sobre

Estes documento README tem como objetivo fornecer as informações necessárias para realização do projeto de avaliação de candidatos. Leia todo antes de começar a desenvolver.

# 🚨 Requisitos: API

- A API deverá ser construída em **NodeJS**
- Caso seja desenvolvida em NodeJS o seu projeto terá que ser implementado em **ExpressJS** ou **NestJS**
- Para a comunicação com o banco de dados utilize algum **ORM**/**ODM**
- Bancos relacionais permitidos:
  - MySQL
  - Postgre
- Bancos não relacionais permitidos:
  - MongoDB
- Sua API deverá seguir os padrões Rest na construção das rotas e retornos
- Sua API deverá conter a collection/variáveis do postman ou algum endpoint da documentação em openapi para a realização do teste
- A sua API pode ser desenvolvida utilizando **JavaScript** ou **TypeScript**

# 🚨 Requisitos: Landing page

- Seu projeto deverá ser construído utilizando **ReactJS** ou **Angular**.
- Seu projeto deverá ser construído utilizando o layout disponibilizado na descrição do teste.
- A integração com a API deve ser feita respeitando todos os contratos de OAuth.
- Projetos utilizando **ReactJS** serão aceitos testes somente em **JavaScript** buscando avaliar o entendimento completo da linguagem e não de estruturas ou dependências que abstraiam determinadas definições não alheias ao ECMAScript.

# 🕵🏻‍♂️ Itens a serem avaliados

- Estrutura do Projeto
- Segurança da API, como SQL Injection e outros
- Boas práticas da Linguagem/Framework
- Seu projeto deverá seguir tudo o que foi exigido na seção [O que desenvolver?](##--o-que-desenvolver)
- Migrations para a criação das tabelas do banco relacional
- Integração com sua API
- Bibliotecas utilizadas
- Estilização dos componentes
- Layout responsivo

# 🎁 Extra

Esses itens não são obrigatórios, porém desejados.

- Linter
- Code Formater
- Testes unitários
- SEO

# 🖥 O que desenvolver?

- Sua API deverá conter dois endpoints:

  1. Cadastro de Pessoas: Endpoint para realizar o cadastro dos participantes do podcast
  2. Listagem de Pessoas: Endpoint para realizar a listagem dos participantes a serem exibidos no site
  3. Registro de Leads: Endpoint para capturar emails de ouvintes do podcast

- A estrutura das tabelas fica por sua conta, pode colocar os campos que quiser desde que façam sentido para a aplicação como um todo.
- Caso necessário, sua aplicação pode fazer integração com outros serviços.
- Você deverá criar uma **Landing page** responsiva seguindo o **layout proposto**
- Integrar seu endpoint de listagem de pessoas
- Integrar seu endpoint de registro de leads

# 🔗 Links e Informações Importantes

## Layout

- Layout e recortes disponíveis no Figma
- https://www.figma.com/file/4gAOwDjzpMsxFS59xzUz6s/Desafio-ioasys-voices-Web?node-id=19%3A0

## Frameworks NodeJS:

- https://expressjs.com/pt-br/
- https://nestjs.com/
